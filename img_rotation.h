#ifndef IMG_ROTATION_H
#define IMG_ROTATION_H

#include <stdlib.h>

#include "image.h"

const struct image rotate(const struct image source);

#endif /* IMG_ROTATION_H */