#include "bmp_img_tools.h"

static enum read_status bmp_check_header( const struct bmp_header* const head ) {
	fprintf(stderr, "checking file header started...\n");

	fprintf(stderr, "check bfType\n");
	if ( head->bfType != BMP_VALID_BFTYPE )
		return READ_INVALID_HEADER;

	fprintf(stderr, "check bfileSize\n");
	if ( head->biSizeImage && ( head->bfileSize != (head->bOffBits + head->biSizeImage) ) )
		return READ_INVALID_HEADER;

	fprintf(stderr, "check biPlanes\n");
	if ( head->biPlanes != 1 )
		return READ_INVALID_HEADER;

	fprintf(stderr, "check biBitCount\n");
	if ( head->biBitCount != 24 )
		return READ_INVALID_HEADER;

	fprintf(stderr, "check biCompression\n");
	if ( head->biCompression != 0 )
		return READ_INVALID_HEADER;

	fprintf(stderr, "checking file header finished...\n");
	fprintf(stderr, "valid header found\n");
	return READ_OK;

}

/*  deserializer */
enum read_status from_bmp( FILE* const in, struct image* img ) {
	struct bmp_header bmp_head = (struct bmp_header) {0};

	// try to read bmp file header
	fprintf(stderr, "trying to read file header\n");
	size_t number_header_bytes_read = fread(&bmp_head, sizeof(struct bmp_header), 1, in);

	// if we have empty header then leave
	fprintf(stderr, "checking if empty header provided\n");
	if ( number_header_bytes_read < 1 ) return READ_INVALID_HEADER;

	// try to check if bitmap header is valid
	fprintf(stderr, "check header structure\n");
	enum read_status header_read_status = bmp_check_header( &bmp_head );

	if ( header_read_status != READ_OK ) return header_read_status;

	fprintf(stderr, "valid BMP header's provided\n");

	// check if we can continue reading the file
	// if result of fseek is non-zero then return error status
	// fprintf(stderr, "check if bitmap provided\n");
	// if ( fseek(in, 0L, SEEK_END) )
	//		return READ_INVALID_SIGNATURE;

	// if ( ftell( in ) != bmp_head.bfileSize )
	//		return READ_INVALID_SIGNATURE;

	// put file position pointer to the start of bit map
	fprintf(stderr, "found bitmap section\n");
	fprintf(stderr, "put pointer to bitmap section of file\n");
	if ( fseek( in, bmp_head.bOffBits, SEEK_SET ) )
		return READ_INVALID_SIGNATURE;

	fprintf(stderr, "deserialization started...\n");
	img->height = bmp_head.biHeight;
	img->width = bmp_head.biWidth;

	fprintf(stderr, "width: %"PRIu32" height: %"PRIu32"\n", img->height, img->width);

	img->data = (struct pixel*) malloc( 
		sizeof( struct pixel ) * img->height * img->width );

	int32_t horizontalOffset = img->width % 4;

	fprintf(stderr, "starting reading the bitmap...\n");
	for (int32_t row = bmp_head.biHeight - 1; row >= 0; --row ) {
		size_t bitmap_bytes_read = fread( img->data + row * bmp_head.biWidth, sizeof(struct pixel), bmp_head.biWidth, in );

		fprintf(stderr, "check if read less than width\n");
		if ( bitmap_bytes_read < bmp_head.biWidth ) {
			fprintf(stderr, "read less bytes than width\n");
			free( img->data );
			return READ_INVALID_BITS;
		}

		fprintf(stderr, "check if we can move to next row\n");
		if ( fseek( in, horizontalOffset, SEEK_CUR ) ) {
			fprintf(stderr, "can't move to the next row\n");
			free( img->data );
            return READ_INVALID_BITS;
        }
	}
	fprintf(stderr, "width: %"PRIu32" height: %"PRIu32"\n", img->height, img->width);
	fprintf(stderr, "deserialization finished successfully...\n");

	return READ_OK;
}


static struct bmp_header create_valid_bmp_header(const struct image* const img) {
	const uint32_t biSizeImagePreCalculated = img->height * (img->width * sizeof(struct pixel) + img->width % 4 );
	const size_t bOffBitsPreCalculated = sizeof(struct bmp_header);

	return (struct bmp_header) {
		.bfType = BMP_VALID_BFTYPE,
		.bOffBits = bOffBitsPreCalculated,
		.biSize = 40,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
		.biWidth = img->width,
		.biHeight = img->height,
		.biSizeImage = biSizeImagePreCalculated,
		.bfileSize = bOffBitsPreCalculated + biSizeImagePreCalculated
	};
}

/*  serializer   */ 
enum write_status to_bmp( FILE* const out, const struct image* const img ) {
	static uint8_t offsetBuffer[] = { 0, 0, 0 };

	struct bmp_header out_bmp_head = ( struct bmp_header ) {0};
	out_bmp_head = create_valid_bmp_header( img );

	fprintf(stderr, "trying to write header\n");
	if ( fwrite( &out_bmp_head, sizeof(struct bmp_header), 1, out ) < 1 ) return WRITE_ERROR;

	int32_t horizontalOffset = img->width % 4;
	fprintf(stderr, "starting writing the bitmap...\n");
	for ( int32_t row = img->height - 1; row >= 0; --row ) {

		fprintf(stderr, "check if we wrote less than row width\n");
		if ( fwrite( img->data + row * img->width, sizeof(struct pixel), img->width, out ) < img->width ) return WRITE_ERROR;

		fprintf(stderr, "check if we reach out the offset\n");
		if ( fwrite( offsetBuffer, 1, horizontalOffset, out ) < horizontalOffset ) return WRITE_ERROR;
	}

	return WRITE_OK;
}