#ifndef BMP_IMG_TOOLS_H
#define BMP_IMG_TOOLS_H

#include  <stdio.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <inttypes.h>
#include  "image.h"

static const uint16_t BMP_VALID_BFTYPE = 0x4d42;

#ifdef _WIN32

/* header of bmp image for Windows */
#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

#else

/* header of bmp image for Linux, Unix, Mac OS */
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

#endif

/*  deserializer */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

/*  deserializer */
enum read_status from_bmp( FILE* const in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

/*  serializer   */
enum write_status to_bmp( FILE* const out, const struct image* const img );

#endif /* BMP_IMG_TOOLS_H */