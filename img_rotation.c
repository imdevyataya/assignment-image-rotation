#include "img_rotation.h"

const struct image rotate(const struct image source ) {
	
	// local variables for a little optimizion
	const uint64_t src_width = source.width;
	const uint64_t src_height = source.height;

	// function result
	struct image new_image = (struct image) { .width = src_height, .height = src_width, .data = NULL };

	// there is a probability of type overflow
	const uint64_t total_pixels = src_width * src_height;

	new_image.data = (struct pixel*) malloc( sizeof(struct pixel) * total_pixels );

	for ( uint32_t y = 0; y < src_height; ++y )
		for ( uint32_t x = 0; x < src_width; ++x )
			new_image.data[x * src_height + y] = source.data[(src_height - y - 1) * src_width + x];
		
	return new_image;
}