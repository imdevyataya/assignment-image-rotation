#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include "bmp_img_tools.h"
#include "img_rotation.h"
#include "utils.h"

typedef const struct image effect( const struct image );

void usage() {
    fprintf(stderr, "usage: flipover [bmp_file_name.bmp]\n"); 
}

// stage 2
void try_open_file_onread(const char* const src_filename, FILE** src_file);

// stage 3
enum read_status file_deserialize_as_bmp(FILE* src_file, struct image* src);

// stage 4: failed
bool is_deserialization_failed(enum read_status status);

// stage 5
void apply_effect(const struct image* const src, struct image* const dist, effect apply);

// stage 6
void try_open_file_onwrite(const char* const dist_filename, FILE** dist_file);

// stage 7
enum write_status file_serialize_as_bmp(FILE* dist_file, struct image* dist);

// stage 8: failed
bool is_serialization_failed(enum write_status status);

void try_close_file(const char* const src_filename, FILE* src_file);

int main(int argc, char** argv) {
	// stage 0: program starts here
	fprintf(stderr, "bmp image rotation program started...\n");

	// stage 1: checking execution arguments
	if (argc < 2) {
		usage();
		err("BMP file name not provided\n");
	}

	// stage 2: opening source file
	FILE* input_file = NULL;
	char* input_filename = argv[1];
	try_open_file_onread(input_filename, &input_file);

	// stage 3: file deserialization
	struct image input_image = (struct image) {0};
	enum read_status file_to_image_status = file_deserialize_as_bmp(input_file, &input_image);
	try_close_file(input_filename, input_file);

	// stage 4: checking convertation status
	if (is_deserialization_failed(file_to_image_status)) {

		// stage 5: print out error message and quit
		switch (file_to_image_status) {
			case READ_INVALID_SIGNATURE:
				err("your file contains invalid signature\n");
			break;
			case READ_INVALID_BITS:
				err("your file contains invalid bitmap\n");
			break;
			case READ_INVALID_HEADER:
				err("your file contains invalid bitmap header structure\n");
			break;
			default:
				err("there is something wrong with your file and we don't know why...\n");
		}

	}

	// stage 5: applying effect
	struct image output_image = (struct image) {0};
	apply_effect(&input_image, &output_image, rotate);

	// stage 6: opening destination file
	const char* output_filename = "out.bmp";
	FILE* output_file = NULL;
	try_open_file_onwrite( output_filename, &output_file );

	// stage 7: file serialization
	enum write_status image_to_file_status = file_serialize_as_bmp(output_file, &output_image);
	try_close_file(output_filename, output_file);

	// stage 8: failed
	if (is_serialization_failed(image_to_file_status)) err("there are errors while recording result\n");

	// stage 8: success
	fprintf(stderr, "image successfully saved\n");
	return 0;
}

void try_open_file_onread(const char* const src_filename, FILE** src_file) {
	fprintf(stderr, "trying to find file named: %s\n", src_filename);
	*src_file = fopen( src_filename, "rb" );
	if ( *src_file == NULL ) err("there are errors while opening input file\n");
}

void try_open_file_onwrite(const char* const dist_filename, FILE** dist_file) {
	fprintf(stderr, "trying to find file named: %s\n", dist_filename);
	*dist_file = fopen( dist_filename, "wb");
	if ( *dist_file == NULL ) err("there are errors while opening output file\n");
}

enum read_status file_deserialize_as_bmp(FILE* src_file, struct image* src) {
	fprintf(stderr, "starting file deserialization...\n");
	enum read_status status = from_bmp( src_file, src );
	fprintf(stderr, "file deserialization finished...\n");
	fprintf(stderr, "width: %"PRIu32" height: %"PRIu32"\n", src->height, src->width);
	return status;
}

bool is_deserialization_failed(enum read_status status) {
	return status != READ_OK;
}

void apply_effect(const struct image* const src, struct image* dist, effect apply) {
	*dist = apply(*src);
}

enum write_status file_serialize_as_bmp(FILE* dist_file, struct image* dist) {
	fprintf(stderr, "starting file serialization...\n");
	enum write_status status = to_bmp( dist_file, dist );
	fprintf(stderr, "file serialization finished...\n");
	return status;
}

bool is_serialization_failed(enum write_status status) {
	return status != WRITE_OK;
}

void try_close_file(const char* const src_filename, FILE* src_file) {
	fprintf(stderr, "trying to close file named: %s\n", src_filename);
	fclose( src_file );
}